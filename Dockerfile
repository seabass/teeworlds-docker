# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: 2021 Sebastian Crane <seabass-labrax@gmx.com>

FROM debian:bullseye
ARG USERID
ARG GROUPID

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y --no-install-recommends libpulse0 teeworlds && \
    rm -rf /var/lib/apt/lists/*

RUN groupadd -g $GROUPID user && \
    adduser user --disabled-password --uid $USERID --gid $GROUPID --gecos ""

USER user
ENV HOME /home/user

ENTRYPOINT ["/usr/games/teeworlds"]
