<!--- SPDX-License-Identifier: CC-BY-SA-4.0 -->
<!--- SPDX-FileCopyrightText: 2021 Sebastian Crane <seabass-labrax@gmx.com> -->

[![REUSE status](https://api.reuse.software/badge/codeberg.org/seabass/teeworlds-docker)](https://api.reuse.software/info/codeberg.org/seabass/teeworlds-docker)
[![CI status](https://ci.codeberg.org/api/badges/7360/status.svg)](https://ci.codeberg.org/seabass/teeworlds-docker)

# teeworlds-docker

Play [Teeworlds](https://teeworlds.com/) inside a container - oop oop oop! 😄

`teeworlds-docker` can run on any host system with an X server and PulseAudio support - that's almost all Linux distributions! By utilising container technologies, you can play Teeworlds without worrying about package conflicts on your host machine, easily sync and share your settings and much more!

## Features

* 🖥️ Windowed or fullscreen display with X11
* 🔉 Music and sounds with PulseAudio
* 🚀 Full graphics acceleration

## Instructions

Make sure you have [Docker](https://docs.docker.com/engine/install/) and [Docker Compose](https://docs.docker.com/compose/install/) installed on your machine. `teeworlds-docker` doesn't require particularly recent versions of these, so you can probably install them with your system's package manager (e.g. `sudo apt-get install docker.io docker-compose` for Debian).

Now you can download and run `teeworlds-docker`:

```bash
git clone https://codeberg.org/seabass/teeworlds-docker
cd teeworlds-docker
./run.sh
```

*Note: [Non-root Podman](https://podman.io/) support coming soon 😉*

## What you can do with it


### Version-controlled Teeworlds settings with branches

Checkout a Git (or another version control system) repository inside the `teeworlds` directory. Now you can have multiple branches containing different settings, skins and friend lists. This is great if you want to have specific keybindings for different game modes. When you find a setting that you want to have in all game modes, you can simply cherry-pick or merge the commit into your other branches! You could even have a 'vanilla' branch to checkout when introducing friends to Teeworlds.

```bash
git clone https://codeberg.org/seabass/teeworlds-docker
cd teeworlds-docker
git init teeworlds
```

### Syncing Teeworlds settings between computers

Make the repository a submodule of your fork of this `teeworlds-docker` repository, and now you can download everything you need to play Teeworlds with a single command.

Assuming you have a fork of this repository at <https://example.org/your-teeworlds-docker-fork> and another repository containing your Teeworlds configuration at <https://example.org/your-teeworlds-config>:

```bash
git clone https://example.org/your-teeworlds-docker-fork teeworlds-docker
cd teeworlds-docker
git submodule add https://example.org/your-teeworlds-config
git push origin master
```

Now, on another computer, you can follow the usual instructions with a couple of changes:

```bash
git clone --recurse-submodules https://example.org/your-teeworlds-docker-fork teeworlds-docker
cd teeworlds-docker
./run.sh
```

### Have fun!

If all that version control stuff isn't your thing, find some friends and just enjoy playing Teeworlds! You don't need to worry about breaking anything, because you can always delete the container and start again 😃

![A Teeworlds character standing next to a tall sunflower](self-portrait.png)

## How it works

The container image, as defined in the Dockerfile, is based on a minimal Debian image. It installs Teeworlds and the PulseAudio client library at build time.

The X Window System and PulseAudio both use a client-server model where one or more clients (such as Teeworlds) communicate over a Unix domain socket to the server, usually a daemon on your computer. With `teeworlds-docker`, the same is still true. By matching the UID and GID of an unprivileged user in the container with that of your user on the host, and by mounting the Unix domain sockets for X and PulseAudio into the container, Teeworlds can display graphics and play sounds the same way as if it were running natively on the host.

Finally, `teeworlds-docker` enables hardware graphics acceleration by simply sharing the [`/dev/dri` device](https://en.wikipedia.org/wiki/Direct_Rendering_Infrastructure) on the host with the container. Due to the excellent work of both [Freedesktop](https://www.freedesktop.org/wiki) and the [Khronos Group](https://www.khronos.org/), it just works 'out of the box'!

## Security implications

`teeworlds-docker` is based on the official Debian image built by Docker Inc. and installs Debian's Teeworlds package at build time. `teeworlds-docker` doesn't download any dependencies from other sources, so the supply chain is pretty trustworthy.

The X Window System and PulseAudio don't sandbox applications, so any binary inside the container could record your display and audio, read your system's clipboard or anything else that you'd usually be able to do on your desktop. File access, however, is limited to the `teeworlds` directory where you started the `teeworlds-docker`, so your `~/.ssh/` directory should be safely out of bounds 🙂

## Feedback

I'd love to hear what you think about `teeworlds-docker`. If you have any suggestions, bug reports or contributions to make, please open a issue or pull request. Feel free to send me an email at `seabass-labrax@gmx.com`!
