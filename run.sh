# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: 2021 Sebastian Crane <seabass-labrax@gmx.com>

#!/bin/sh

mkdir -p teeworlds/maps
USERID=$(id -u) GROUPID=$(id -g) docker-compose up --force-recreate
